import React from "react";

const JitsiHeader = () => {
  return (
    <div className="jitsi__header">
      <div className="jitsi__header__left">
        <i class="fas fa-info"></i>
      </div>
      <div className="jitsi__header__right"></div>
    </div>
  );
};

export default JitsiHeader;
