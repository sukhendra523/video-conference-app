import React, { useEffect } from "react";
import PropTypes from "prop-types";

import useJitsi from "../useJitsi";

// import CallEndIcon from "@material-ui/icons/CallEnd";
// import MicOffIcon from "@material-ui/icons/MicOff";
// import VideocamOffIcon from "@material-ui/icons/VideocamOff";
// import MicNoneIcon from "@material-ui/icons/MicNone";
// import VideocamIcon from "@material-ui/icons/Videocam";
// import AppsIcon from "@material-ui/icons/Apps";
// import AppsOutlinedIcon from "@material-ui/icons/AppsOutlined";
// import DesktopAccessDisabledIcon from "@material-ui/icons/DesktopAccessDisabled";
// import DesktopWindowsIcon from "@material-ui/icons/DesktopWindows";

import "./Meet.css";
import Toolbox from "./Toolbox";

const Meet = ({
  roomName,
  displayName,
  password,
  // loadingComponent,
  // errorComponent,
  // containerStyles,
  // jitsiContainerStyles,
  // onConnection,
  // onError,
  ...uioptions
}) => {
  console.log(
    "RoomName 🛩🛩🛩🛩🛩🌝🌫🌝🌫🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝",
    roomName
  );
  const { loading, error, connection } = useJitsi({
    parentNode: "jitsi-container",
    roomName,
    displayName,
    password,
    ...uioptions,
  });

  // useEffect(() => {
  //   if (connection && onConnection) onConnection(connection);
  // }, [connection]);

  // useEffect(() => {
  //   if (error && onError) onError(error);
  // }, [error]);

  return (
    <>
      <div id="audioOutputSelectWrapper" style={{ display: "none" }}>
        Change audio output device
        <select id="audioOutputSelect"></select>
      </div>

      <div className="container meet">
        <div id="jitsi-container">
          {/* <div className="mic">
            <i className="fas fa-microphone"></i>
          </div>
          <div className="video-off">
            <img className="avatar" src="images/avatar.svg" />
          </div> */}
        </div>

        <div id="participants_info" style={{ display: "none" }}></div>
        <Toolbox />
      </div>
    </>
  );
};

Meet.propTypes = {
  jwt: PropTypes.string,
  domain: PropTypes.string,
  subject: PropTypes.string,
  password: PropTypes.string,
  roomName: PropTypes.string.isRequired,
  displayName: PropTypes.string,
  loadingComponent: PropTypes.object,
  errorComponent: PropTypes.object,
  containerStyles: PropTypes.object,
  jitsiContainerStyles: PropTypes.object,
  configOverwrite: PropTypes.object,
  interfaceConfigOverwrite: PropTypes.object,
  onError: PropTypes.func,
  onJitsi: PropTypes.func,
};

export default Meet;
