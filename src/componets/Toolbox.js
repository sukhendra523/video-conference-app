import React from "react";

const Toolbox = () => {
  return (
    <div id="toolbox" className="toolbox">
      <div className="toolbox__left">
        <button id="btnCustomCamera">
          <i className="fas fa-video"></i>
        </button>
        <button id="btnCustomMic">
          <i className="fas fa-microphone"></i>
        </button>
      </div>
      <div className="toolbox__center">
        <button id="btnSecurity">
          <i className="fas fa-shield-alt"></i>
        </button>
        <button id="btnParticipants">
          <i className="fas fa-user-friends"></i>
        </button>
        <button id="btnChatbox">
          <i className="fas fa-comment-dots"></i>
        </button>
        <button id="btnCustomTileView" style={{ color: "green" }}>
          <i className="fas fa-th-large"></i>
        </button>
        <button id="btnScreenShareCustom">
          <i className="fas fa-arrow-alt-circle-up"></i>
        </button>

        <button id="btnStartRecording">
          <i className="fas fa-compact-disc"></i>
        </button>

        <button id="btnReaction">
          <i className="fas fa-grin-stars"></i>
        </button>
      </div>
      <div className="toolbox__right">
        <button id="btnHangup">
          <i className="fas fa-phone"></i>
        </button>
      </div>
    </div>
  );
};

export default Toolbox;
