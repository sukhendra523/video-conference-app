import React, { useEffect, useState } from "react";
require("dotenv").config();

const useJitsi = ({
  domain = process.env.domain,
  roomName = "learnig523jitsi",
  displayName = "Guest",
  password,
  parentNode,
  // subject,
  ...uioptions
}) => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [connection, setConnection] = useState(null);

  useEffect(() => {
    function toggleTrack(trackType) {
      let type = { video: 1, audio: 0 };
      if (!localTracks[type[trackType]].isMuted()) {
        localTracks[type[trackType]].mute();
      } else {
        localTracks[type[trackType]].unmute();
      }
    }

    function unload() {
      $("body").html(
        '<div class="meet-end"><img src="images/ty.jpg"><a class="start-again" href="http://localhost:3000/">Start Meeting Again</a></div>'
      );
      for (let i = 0; i < localTracks.length; i++) {
        localTracks[i].dispose();
      }
      room.leave();
      connection.disconnect();
    }

    let isVideo = true;

    function toggleTileView() {
      $("#jitsi-container").toggleClass("tile__view");
      $(".video").toggleClass("tile__view__video");
    }

    function switchVideo() {
      // eslint-disable-line no-unused-vars
      isVideo = !isVideo;
      if (localTracks[1]) {
        localTracks[1].dispose();
        localTracks.pop();
      }
      JitsiMeetJS.createLocalTracks({
        devices: [isVideo ? "video" : "desktop"],
      })
        .then((tracks) => {
          localTracks.push(tracks[0]);
          localTracks[1].addEventListener(
            JitsiMeetJS.events.track.TRACK_MUTE_CHANGED,
            () => console.log("local track muted")
          );
          localTracks[1].addEventListener(
            JitsiMeetJS.events.track.LOCAL_TRACK_STOPPED,
            () => console.log("local track stoped")
          );
          localTracks[1].attach($("#localVideo1")[0]);
          room.addTrack(localTracks[1]);
        })
        .catch((error) => console.log(error));
    }

    function BindEvent() {
      $("#btnHangup").on("click", function () {
        unload();
      });
      $("#btnCustomMic").on("click", function () {
        toggleTrack("audio");
      });
      $("#btnCustomCamera").on("click", function () {
        toggleTrack("video");
      });
      $("#btnCustomTileView").on("click", function () {
        toggleTileView();
      });
      $("#btnScreenShareCustom").on("click", function () {
        switchVideo();
      });
      $("#audioOutputSelect").on("change", function () {
        changeAudioOutput(this);
      });
      // $("#btnChatbox").on("click", function () {

      // });
    }

    BindEvent();

    const options = {
      serviceUrl: process.env.serviceUrl,
      hosts: {
        domain: domain,
        muc: process.env.muc,
      },
      resolution: 1080,
      maxFullResolutionParticipants: 2,
      setSenderVideoConstraint: "1080",
      setReceiverVideoConstraint: "180",
      constraints: {
        video: {
          aspectRatio: 16 / 9,
          height: {
            ideal: 1080,
            max: 1080,
            min: 1080,
          },
        },
      },
    };

    if (!window.JitsiMeetJS) {
      setError(
        "JitsiMeetJS is not available, check if https://test-comm.encade.org/libs/lib-jitsi-meet.min.js was loaded"
      );
      return;
    }

    uioptions.parentNode = document.getElementById(parentNode);
    if (!uioptions.parentNode) {
      setError(
        `Parent node is not available, check container have the correct id: "${parentNode}"`
      );
      return;
    }
    const confOptions = {
      openBridgeChannel: true,
    };

    let connection = null;
    let isJoined = false;
    let room = null;

    let localTracks = [];
    const remoteTracks = {};

    /**
     * Handles local tracks.
     * @param tracks Array with JitsiTrack objects
     */
    function onLocalTracks(tracks) {
      localTracks = tracks;
      for (let i = 0; i < localTracks.length; i++) {
        localTracks[i].addEventListener(
          JitsiMeetJS.events.track.TRACK_AUDIO_LEVEL_CHANGED,
          (audioLevel) => console.log(`Audio Level local: ${audioLevel}`)
        );
        localTracks[i].addEventListener(
          JitsiMeetJS.events.track.TRACK_MUTE_CHANGED,
          () => {
            console.log("local track muted");
            if (localTracks[i].getType() === "video") {
              if (localTracks[i].isMuted()) {
                $("#btnCustomCamera").html(
                  '<i class="fas fa-video-slash"></i>'
                );
                $(`#${parentNode} .localVideo${i} .video-off`).css(
                  "display",
                  "flex"
                );
              } else {
                $("#btnCustomCamera").html('<i class="fas fa-video"></i>');
                $(`#${parentNode} .localVideo${i} .video-off`).css(
                  "display",
                  "none"
                );
              }
            } else {
              if (localTracks[i].isMuted()) {
                $("#btnCustomMic").html(
                  '<i class="fas fa-microphone-slash"></i>'
                );
                $(`.local-video .mic`).html(
                  '<i class="fas fa-microphone-slash"></i>'
                );
              } else {
                $("#btnCustomMic").html('<i class="fas fa-microphone"></i>');
                $(`.local-video .mic`).html(
                  '<i class="fas fa-microphone"></i>'
                );
              }
            }
          }
        );

        localTracks[i].addEventListener(
          JitsiMeetJS.events.track.LOCAL_TRACK_STOPPED,
          () => console.log("local track stoped")
        );
        localTracks[i].addEventListener(
          JitsiMeetJS.events.track.TRACK_AUDIO_OUTPUT_CHANGED,
          (deviceId) =>
            console.log(`track audio output device was changed to ${deviceId}`)
        );

        if (localTracks[i].getType() === "video") {
          $(`#${parentNode}`).append(
            `<div class='video local-video localVideo${i}'>
            <div class="participantName"><h5>${displayName}</h5></div>
            <div class="mic"><i class="fas fa-microphone"></i></div>
            <div class="video-off">
            <img class="avatar" src="images/avatar.svg" />
            </div>
            <video autoplay='1' id='localVideo${i}' />
            </div>`
          );

          localTracks[i].attach($(`#localVideo${i}`)[0]);
        } else {
          $(`#${parentNode}`).append(
            `<div class='localAudio${i}'><audio autoplay='1' muted='true' id='localAudio${i}' /></div>`
          );
          localTracks[i].attach($(`#localAudio${i}`)[0]);
        }
        if (isJoined) {
          room.addTrack(localTracks[i]);
        }
      }
    }

    /**
     * Handles remote tracks
     * @param track JitsiTrack object
     */
    function onRemoteTrack(track) {
      if (track.isLocal()) {
        return;
      }
      const participant = track.getParticipantId();

      if (!remoteTracks[participant]) {
        remoteTracks[participant] = [];
      }
      const idx = remoteTracks[participant].push(track);

      track.addEventListener(
        JitsiMeetJS.events.track.TRACK_AUDIO_LEVEL_CHANGED,
        (audioLevel) => console.log(`Audio Level remote: ${audioLevel}`)
      );
      track.addEventListener(
        JitsiMeetJS.events.track.TRACK_MUTE_CHANGED,
        () => {
          console.log("remote track muted");

          if (track.getType() === "video") {
            if (track.isMuted()) {
              $(`#${parentNode} .${participant}video${idx} .video-off`).css(
                "display",
                "flex"
              );
            } else {
              $(`#${parentNode} .${participant}video${idx} .video-off`).css(
                "display",
                "none"
              );
            }
          } else {
            if (track.isMuted()) {
              $(`.remote-video .mic`).html(
                '<i class="fas fa-microphone-slash"></i>'
              );
            } else {
              $(`.remote-video .mic`).html('<i class="fas fa-microphone"></i>');
            }
          }
        }
      );

      track.addEventListener(JitsiMeetJS.events.track.LOCAL_TRACK_STOPPED, () =>
        console.log("remote track stoped")
      );

      track.addEventListener(
        JitsiMeetJS.events.track.TRACK_AUDIO_OUTPUT_CHANGED,
        (deviceId) =>
          console.log(`track audio output device was changed to ${deviceId}`)
      );

      const id = participant + track.getType() + idx;
      let participantName = room.getParticipantById(participant)._displayName;

      console.log("participantName🌞🌞🌞", participantName);

      if (track.getType() === "video") {
        $(`#${parentNode}`).append(
          `<div class="video remote-video ${participant}video${idx}">
          <div class="participantName"><h5>${participantName}</h5></div>
          <div class="mic"><i class="fas fa-microphone"></i></div>
          
            <div class="video-off">
            <img class="avatar" src="images/avatar.svg" />
            </div>
          <video autoplay='1' id='${participant}video${idx}' />
          </div>`
        );
      } else {
        $(`#${parentNode}`).append(
          `<audio autoplay='1' id='${participant}audio${idx}' />`
        );
      }
      track.attach($(`#${id}`)[0]);
    }

    /**
     * That function is executed when the conference is joined
     */
    function onConferenceJoined() {
      console.log("conference joined!");
      isJoined = true;
      for (let i = 0; i < localTracks.length; i++) {
        room.addTrack(localTracks[i]);
      }
    }

    /**
     *
     * @param id
     */
    function onUserLeft(id) {
      console.log("user left");
      if (!remoteTracks[id]) {
        return;
      }
      const tracks = remoteTracks[id];

      for (let i = 0; i < tracks.length; i++) {
        tracks[i].detach($(`#${id}${tracks[i].getType()}`));
      }
    }

    /**
     * That function is called when connection is established successfully
     */
    console.log(
      "RoomName 🛩🛩🛩🛩🛩🌝🌫🌝🌫🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝🌝",
      roomName
    );
    function onConnectionSuccess() {
      //roomname learnigjitsisukh learnig523jitsi
      room = connection.initJitsiConference(roomName, confOptions);
      if (displayName) {
        room.setDisplayName(displayName);
      }

      room.on(JitsiMeetJS.events.conference.TRACK_ADDED, onRemoteTrack);
      room.on(JitsiMeetJS.events.conference.TRACK_REMOVED, (track) => {
        console.log(`track removed!!!${track}`);
      });
      room.on(
        JitsiMeetJS.events.conference.CONFERENCE_JOINED,
        onConferenceJoined
      );
      room.on(JitsiMeetJS.events.conference.USER_JOINED, (id) => {
        console.log("user join");
        remoteTracks[id] = [];
      });
      room.on(JitsiMeetJS.events.conference.USER_LEFT, onUserLeft);
      room.on(JitsiMeetJS.events.conference.TRACK_MUTE_CHANGED, (track) => {
        console.log(`${track.getType()} - ${track.isMuted()}`);
      });
      room.on(
        JitsiMeetJS.events.conference.DISPLAY_NAME_CHANGED,
        (userID, displayName) =>
          console.log(
            "${userID} - ${displayName} 🌞🌞🌞"`${userID} - ${displayName}`
          )
      );
      room.on(
        JitsiMeetJS.events.conference.TRACK_AUDIO_LEVEL_CHANGED,
        (userID, audioLevel) => console.log(`${userID} - ${audioLevel}`)
      );
      room.on(JitsiMeetJS.events.conference.PHONE_NUMBER_CHANGED, () =>
        console.log(`${room.getPhoneNumber()} - ${room.getPhonePin()}`)
      );
      room.join();
    }

    /**
     * This function is called when the connection fail.
     */
    function onConnectionFailed() {
      console.error("Connection Failed!");
    }

    /**
     * This function is called when the connection fail.
     */
    function onDeviceListChanged(devices) {
      console.info("current devices", devices);
    }

    /**
     * This function is called when we disconnect.
     */
    function disconnect() {
      console.log("disconnect!");
      connection.removeEventListener(
        JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED,
        onConnectionSuccess
      );
      connection.removeEventListener(
        JitsiMeetJS.events.connection.CONNECTION_FAILED,
        onConnectionFailed
      );
      connection.removeEventListener(
        JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED,
        disconnect
      );
    }

    /**
     *
     */

    /**
     *
     */
    // function toggleTrack(trackType) {
    //   // eslint-disable-line no-unused-vars
    //   for (let i = 0; i < localTracks.length; i++) {
    //     if (localTracks[i].getType == trackType) {
    //       if (!localTracks[1].isMuted()) {
    //         localTracks[1].mute();
    //       } else {
    //         localTracks[1].unmute();
    //       }
    //     } else {
    //       continue;
    //     }
    //   }
    // }

    function toggleVideo() {
      // eslint-disable-line no-unused-vars
      if (!localTracks[1].isMuted()) {
        localTracks[1].mute();
        $("#btnCustomCamera").html('<i class="fas fa-video-slash"></i>');
        $(".container .video-off").css("display", "flex");
      } else {
        localTracks[1].unmute();
        $("#btnCustomCamera").html('<i class="fas fa-video"></i>');
        $(".container .video-off").css("display", "none");
      }
    }

    // function toggleAudio() {
    //   // eslint-disable-line no-unused-vars
    //   if (!localTracks[0].isMuted()) {
    //     localTracks[0].mute();
    //     $("#btnCustomMic").html('<i class="fas fa-microphone-slash"></i>');
    //     $("#jitsi-container .mic").html(
    //       '<i class="fas fa-microphone-slash"></i>'
    //     );
    //   } else {
    //     localTracks[0].unmute();
    //     $("#btnCustomMic").html('<i class="fas fa-microphone"></i>');
    //     $("#jitsi-container .mic").html('<i class="fas fa-microphone"></i>');
    //   }
    // }

    /**
     *
     * @param selected
     */
    function changeAudioOutput(selected) {
      // eslint-disable-line no-unused-vars
      JitsiMeetJS.mediaDevices.setAudioOutputDevice(selected.value);
    }

    $(window).bind("beforeunload", unload);
    $(window).bind("unload", unload);

    // JitsiMeetJS.setLogLevel(JitsiMeetJS.logLevels.ERROR);
    const initOptions = {
      disableAudioLevels: true,
    };

    JitsiMeetJS.init(initOptions);

    connection = new JitsiMeetJS.JitsiConnection(null, null, options);
    setConnection(connection);
    setLoading(false);
    setError(null);

    connection.addEventListener(
      JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED,
      onConnectionSuccess
    );
    connection.addEventListener(
      JitsiMeetJS.events.connection.CONNECTION_FAILED,
      onConnectionFailed
    );
    connection.addEventListener(
      JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED,
      disconnect
    );

    JitsiMeetJS.mediaDevices.addEventListener(
      JitsiMeetJS.events.mediaDevices.DEVICE_LIST_CHANGED,
      onDeviceListChanged
    );

    connection.connect();

    JitsiMeetJS.createLocalTracks({ devices: ["audio", "video", "name"] })
      .then(onLocalTracks)
      .catch((error) => {
        throw error;
      });

    // if (JitsiMeetJS.mediaDevices.isDeviceChangeAvailable("output")) {
    //   JitsiMeetJS.mediaDevices.enumerateDevices((devices) => {
    //     const audioOutputDevices = devices.filter(
    //       (d) => d.kind === "audiooutput"
    //     );

    //     if (audioOutputDevices.length > 1) {
    //       $("#audioOutputSelect").html(
    //         audioOutputDevices
    //           .map((d) => `<option value="${d.deviceId}">${d.label}</option>`)
    //           .join("\n")
    //       );

    //       $("#audioOutputSelectWrapper").show();
    //     }
    //   });
    // }
  }, [window.JitsiMeetJS]);
  /* global $, JitsiMeetJS */
  return { connection, error, loading };
};

export default useJitsi;
